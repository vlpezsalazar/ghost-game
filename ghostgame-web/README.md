**Piksel Coding Challenge: Ghost Game.**

This software is a Java web application that provides a basic GUI for a human to play against the optimal computer player for the game of Ghost. In this game, two players take turns building up an English word from left to right. Each player adds one letter per turn. The goal is to not complete the spelling of a word: if you add a letter that completes a word (of 4+ letters), or if you add a letter that produces a string that cannot be extended into a word, you lose.

The game can be played from inside the Firefox browser and make use of AJAX to update the page as the game progresses.