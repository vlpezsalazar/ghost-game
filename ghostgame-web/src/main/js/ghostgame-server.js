/*global define*/
/**
 * Client interface to communicate with the server.
 */
define(function(){
	"use strict";
    //private
    var conn;
    // public server API
    return {
        init : function(/*XMLHttpRequest*/xmlconn, /*function*/controller){
            conn = xmlconn;
            conn.onreadystatechange = controller;
        },
        /**
         * Callback Generator for a View.
         * Sends a describe game module request to the server. 
         * Gets the moduleId request parameter from the select view.
         * @param a View module
         */
        describeCallbackFor : function(/*View*/View){
            return function(){
                conn.open("POST", "describe", true);
                conn.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                conn.setRequestHeader("Accept", "application/json");
                conn.send("moduleId="+encodeURIComponent(View.getModuleId()));

            };
        },
        /**
         * Callback Generator for a View.
         * Sends a start new game request to the server.
         * Gets the moduleId request parameter from the select view.
         */
        startNewGameCallbackFor : function(/*View*/View){
            return function(){
                conn.open("POST", "startNewGame", true);
                conn.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                conn.setRequestHeader("Accept", "application/json");
                conn.send("moduleId="+encodeURIComponent(View.getModuleId()));
            };
        },
        /**
         * Callback Generator for a View.
         * Sends a play request to the server.
         * Gets the move request parameter from the word view.
         */
        playCallbackFor : function(/*View*/View){
            return function(){
                conn.open("POST", "play", true);
                conn.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                conn.setRequestHeader("Accept", "application/json");
                conn.send("move=" + View.getLastWordCharacter());
            };
        }
    };
});
