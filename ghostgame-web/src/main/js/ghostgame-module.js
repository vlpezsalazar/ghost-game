/*global define*/
define(["ghostgame-view", "ghostgame-server"], function(View, Server){
	"use strict";
    //private var
    var conn = new XMLHttpRequest();

    //public module API
    return {
        /**
         * Constructor. Initializes the module.
         *
         * @param [string] button the id of the start new game button. 
         *        [string] gameModules the id of the select where gamemodules are depicted.
         *        [string] log the id of the div used for the server messages.
         *        [string] moduleDescription the id of the div used to hold the modules description. 
         *        [string] IADescription the id of the div used to hold the IA description.
         *        [string] word the id of the input text used to depict the current word.
         */
        init : function(/*string*/ button, /*string*/ gameModules, /*string*/ log, /*string*/ moduleDescription, /*string*/ IADescription, /*string*/ word){
            //init view
            View.init(button, gameModules,log, moduleDescription, IADescription, word);
            // set the view controller and request object
            Server.init(conn, View.controller(conn));

            //add triggers for the view events
            View.setCallbacks(Server.startNewGameCallbackFor(View),
                              Server.playCallbackFor(View),
                              Server.describeCallbackFor(View));
        }
        
    };
}());
