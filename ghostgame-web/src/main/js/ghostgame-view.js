/*global define*/
/**
 * View interface.
 */
define(function(){
    // public View API
    return {
        init : function(/*string*/ button, /*string*/ gameModules, /*string*/ log, /*string*/ moduleDescription, /*string*/ IADescription, /*string*/ word){
            //populate view elements
            this.button = document.querySelector("#" +button);
            this.select = document.querySelector("#" +gameModules);
            this.log = document.querySelector("#" +log);
            this.moduleDescription = document.querySelector("#" +moduleDescription);
            this.IADescription = document.querySelector("#" +IADescription);
            this.word = document.querySelector("#" +word);
            this.gameStarted = false;
        },
        setCallbacks : function(/*function*/ newGameCallback, /*function*/playCallback, /*function*/describeCallback){
        	var that = this;
            this.button.onclick = (function(){
            	return function(){
            		that.gameStarted = true;
	            	that.clearLog();
	            	that.clearWord();
	            	newGameCallback();
            	}
            }());
            this.select.onchange = describeCallback;
            this.word.onkeydown = function(){
            	if (!that.gameStarted){
        			that.setLog("You should start a new game first...");
        		}
            }
            this.word.onkeyup = playCallback; 
        },
        clearLog : function(){
        	this.setLog("");
        },
        setLog : function(/*string*/ text){
            var log = this.log;
            if (typeof log !== "undefined"){
                log.innerHTML = text;
            }
        },
        setIADescription : function(/*string*/ text){
            var desc = this.IADescription;
            if (typeof desc !== "undefined"){
                desc.innerHTML = text;
            }
        },
        setModuleDescription : function(/*string*/ text){
            var desc = this.moduleDescription;
            if (typeof desc !== "undefined"){
                desc.innerHTML = text;
            }
        },
        setCurrentWord : function(/*string*/ text){
            var word = this.word;
            if (typeof word !== "undefined"){
                word.value = text;
            }
        },
        getModuleId : function(){
            return this.select.value;
        },
        clearWord : function(){
        	return this.word.value = "";
        },
        getLastWordCharacter : function() {
            return this.word.value[this.word.value.length-1];
        },
        controller : function(/*XMLHttpRequest*/conn){
            var View = this;
            return function() {
                if (conn.readyState==4 && conn.status==200){
                    var response = JSON.parse(conn.responseText);
                    if (typeof response.reason !== "undefined"){
                        View.setLog(response.reason);
                    }
                    if (typeof response.iadescription !== "undefined"){
                        View.setIADescription(response.iadescription);
                    }
                    if (typeof response.rulesDescription !== "undefined"){
                        View.setModuleDescription(response.rulesDescription);
                    }
                    if (typeof response.currentWord !== "undefined"){
                        View.setCurrentWord(response.currentWord);
                    }
                }
            };
        }
    };
});
