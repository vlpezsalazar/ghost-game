<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.IGhostGameModules"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>

<html>
<head>
	<title>Piksel Java Lead Developer Position - Coding Challenge</TITLE>
	<script type = "text/javascript" src = "js/bundle.js"></script>
</head>
<body>
<table style="width:100%;border:0">
<tbody>
    <tr>
        <td colspan="3" align="center">
            <H1>Ghost Game!</H1>
        </td>
    </tr>
    <tr>
         <td colspan="3" align="center">
            <h2>Select your opponent and play. Good luck!</h2>
        </td>
    </tr>

    <tr>
        <td>
            <form:select path="ghostGameModules"  id="gameModules" size ="4">
                <form:option value="" label="select one of the following"/>
                <form:options items="${ghostGameModules.getModulesIds()}"/>
            </form:select>
        </td>
        <td>
        <div>
            <div id = "moduleDescription" style = "border: 1px; width : 200px; heigth : 200px"></div>
            <div id = "IADescription" style = "border: 1px; width : 200px; heigth : 100px" ></div>
        </div>
        </td>
        <td align="center">
            <input id="word" type="text"/>
        </td>
    </tr> 
    <tr>
        <td></td><td></td>
        <td align="center">
            <input type="button" id="newgame" value="Start New Game!"/>
        </td>

    </tr>
    <tr>
        <td colspan="3" align="center">
            <div id = "log" style = "border: 1px; width : 200px; heigth : 200px"></div>
        </td>
    </tr>
</tbody>
</table>
</body>
    <script type="text/javascript">
        require(["ghostgame-module"], function(gmvc){
            gmvc.init("newgame", "gameModules", "log", "moduleDescription", "IADescription", "word");        	
        });
    </script>
</html>

