package victor.lopez.coding.challenge.java.lead.developer.in.malaga.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.GhostGameModules;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.IGhostGameModules;

@Configuration
public class ModulesConfig {

    @Bean(name = "ghostGameModules")
    IGhostGameModules ghostGameModules() {
        return new GhostGameModules(); 
    }
}
