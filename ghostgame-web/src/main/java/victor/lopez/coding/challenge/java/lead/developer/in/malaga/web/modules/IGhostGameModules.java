package victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules;

import java.util.Map;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.exceptions.NoSuchModuleException;

public interface IGhostGameModules {

    void load(String realPath);
    
    void reload();

    <T> T getInstance(String moduleFactoryId, Class<T> clazz) throws NoSuchModuleException;

    Map<String, String> getModulesIds();
}
