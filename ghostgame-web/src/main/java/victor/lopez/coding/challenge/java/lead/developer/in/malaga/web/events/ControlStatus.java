package victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.events;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGameState;

public class ControlStatus implements IGameState {
    public static final ControlStatus MODULE_FAILED = new ControlStatus("There is no module named that way!", true);

    private String reason;
    private boolean ended;
    private ControlStatus(String reason, boolean ended) {
        this.reason = reason;
        this.ended = ended;
    }
    @Override
    public String getReason() {
        return reason;
    }
    @Override
    public boolean isGameEnded() {
        return ended;
    }
    @Override
    public String getCurrentWord() {
        return null;
    }
    
}
