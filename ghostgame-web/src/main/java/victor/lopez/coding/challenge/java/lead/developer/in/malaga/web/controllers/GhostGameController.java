package victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.GameModuleDescription;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGameState;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostPlayer;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.exceptions.NoSuchWordException;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.events.ControlStatus;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.exceptions.NoSuchModuleException;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.IGhostGameModules;

@RequestMapping(produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
@Controller
public class GhostGameController {
    
    private static final String MODULE = "module";
    private static final String PLAYER = "player";
    private static final String SECRET = "secret";
    private static final String SECRETVALUE = "DMldkfsjdlfj232232,3##11";

    private static final GameModuleDescription NOSUCHGAMEMODULE = new GameModuleDescription("", "There is no game module with that id.");
    
    @Autowired
    private IGhostGameModules gameModules;
    
    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String toHome() {
        return "index";
    }
    
    @RequestMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = {RequestMethod.POST}, value ="describe")
    public @ResponseBody GameModuleDescription describe(@RequestParam(value = "moduleId", required = true)String moduleId) {
        GameModuleDescription response;
            try {
                response = getGameModules().getInstance(moduleId, GameModuleDescription.class);
            } catch (NoSuchModuleException e) {
                response = NOSUCHGAMEMODULE;
            }
        return response;
    }
    
    @RequestMapping(method = {RequestMethod.POST}, value = "startNewGame")
    public @ResponseBody IGameState starNewGame(@RequestParam(value = "moduleId", required = true)String moduleId, HttpSession session) {
        IGameState response;
        try {
            IGhostGameRulesModule module = getGameModules().getInstance(moduleId, IGhostGameRulesModule.class); 
            session.setAttribute(PLAYER, getGameModules().getInstance(moduleId, IGhostPlayer.class));
            session.setAttribute(MODULE, module);
            response = module.startNewGame();
        } catch (NoSuchModuleException e) {
            response = ControlStatus.MODULE_FAILED;
        }
        return response;
    }
    
    @RequestMapping(method = {RequestMethod.POST}, value = "play")
    public @ResponseBody IGameState play(@RequestParam(value = "move", required = true)String move, HttpSession session) throws NoSuchWordException {
        IGhostPlayer player = (IGhostPlayer) session.getAttribute(PLAYER);
        IGhostGameRulesModule module = (IGhostGameRulesModule) session.getAttribute(MODULE);
        IGameState state = null;
        if (module  != null) {
            state = module.nextTurn(move);
            if (!state.isGameEnded()) {
                String playerMove = player.play(module.getCurrentWord());
                state = module.nextTurn(playerMove);
            }
        }
        return state;
    }
    @RequestMapping(method = {RequestMethod.PUT}, value = "update")
    public void update(@RequestParam(value = SECRET, required = true) String securityToken){
        if (SECRETVALUE.equals(SECRET)) {
            //Instructs the server to load changed modules using the same path
            getGameModules().reload();
        }
    }
    public IGhostGameModules getGameModules() {
        return gameModules;
    }

    public void setGameModules(IGhostGameModules modules) {
        this.gameModules = modules;
    }
}
