package victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ServletContextAware;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.exceptions.NoSuchModuleException;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class GhostGameModules implements IGhostGameModules, ServletContextAware, InitializingBean {

    //deployment path in the server for game-modules
    private static final String GAMEMODULEPATHS = "/game-modules/";

    @Autowired
    private ServletContext servletContext;

    // loaded modules
    public LinkedHashMap<String, Injector> factories = new LinkedHashMap<String, Injector>();

    private String lastPath = null;

    public GhostGameModules() {}

    public GhostGameModules(String path) {
        lastPath = path;
    }

    public void load(String file) {
        lastPath = file;
        reload();
    }

    public void reload() {
        if (lastPath != null) {
            File modulesPath = new File(lastPath);
            if (modulesPath.isDirectory()) {
                factories.clear();
                for (File module : modulesPath.listFiles()) {
                    URLClassLoader urlClassLoader = null;
                    try {
                        urlClassLoader = new URLClassLoader(new URL[]{module.toURI().toURL()}, this.getClass().getClassLoader());
                        ServiceLoader<AbstractModule> loader = ServiceLoader.load(AbstractModule.class, urlClassLoader);
                        Iterator<AbstractModule> iterator = loader.iterator();
                        if (iterator.hasNext()) {
                            Injector factory = Guice.createInjector(iterator.next());
                            factories.put(module.getName(), factory);
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        log.error("", e);
                    }
                }
            }
        } else {
            log.error("The path to reload the modules was null!");
        }
    }
    @Override
    public Map<String, String> getModulesIds() {
        Map<String, String> modules = new HashMap<String, String>();
        for (String module : factories.keySet()) {
            modules.put(module, module);
        }
        return modules;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        if (lastPath == null) {
            lastPath = servletContext.getRealPath(GAMEMODULEPATHS);
            reload();
        }
    }
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    public <T> T getInstance(String moduleFactoryId, Class<T> clazz) throws NoSuchModuleException {
        Injector injector = factories.get(moduleFactoryId);
        if (injector == null) throw new NoSuchModuleException();
        return injector.getInstance(clazz);
    }
    private Logger log = LoggerFactory.getLogger(GhostGameModules.class);

}
