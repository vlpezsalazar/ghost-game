/*global define, jasmine*/
define(["ghostgame-server"], function(Server) {
    describe("A Server", function() {
        var MockView, controller = function(){};
        beforeEach(function() {
            MockView = {
                getModuleId : function(){
                    return "mock";
                },
                getLastWordCharacter : function(){
                    return "a";
                }
            };
            
            jasmine.Ajax.useMock();
            Server.init(jasmine.Ajax.jQueryMock(), controller);
        });

        it("should request start new game with param moduleId", function() {
            Server.startNewGameCallbackFor(MockView)();
            expect(mostRecentAjaxRequest().url).toBe('startNewGame');
        });
        it("should request describe with param moduleId", function() {
            Server.describeCallbackFor(MockView)();
            expect(mostRecentAjaxRequest().url).toBe('describe');
        });
        it("should request start new game with param move", function() {
            Server.playCallbackFor(MockView)();
            expect(mostRecentAjaxRequest().url).toBe('play');
        });
  });

  
});