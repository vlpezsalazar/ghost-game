/*global define, jasmine*/
define(["ghostgame-view"], function(View) {
    jasmine.getFixtures().fixturesPath = "/spec/fixtures";
    describe("On View", function() {
        beforeEach(function() {
            loadFixtures("fixture.html");
            View.init("newgame", "gameModules", "log", "moduleDescription", "IADescription", "word");            
        });
        it("should set 'hello World!' in log when setLog()", function() {
            View.setLog('hello World!');
            expect($("#log")).toHaveText("hello World!");
        });
        it("should set 'hello World!' in moduleDescription' when setModuleDescription()", function() {
            View.setModuleDescription('hello World!');
            expect($("#moduleDescription")).toHaveText("hello World!");
        });
        it("should set 'hello World!' in IADescription' when setIADescription()", function() {
            View.setIADescription('hello World!');
            expect($("#IADescription")).toHaveText("hello World!");
        });
        it("should set 'hello World!' in word' when setCurrentWord()", function() {
            View.setCurrentWord('hello World!');
            expect($("#word")).toHaveValue("hello World!");
        });
        it("should return 'o1' for getModuleId()", function() {
            expect(View.getModuleId()).toBe("o1");
        });
        it("should return 'v' for getLastWordCharacter()", function() {
            expect(View.getLastWordCharacter()).toBe("v");
        });
        
  });

  
});