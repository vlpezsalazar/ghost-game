package integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.WebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { WebConfig.class })
public class WebIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void thatTextReturned() throws Exception {
        MockHttpServletRequestBuilder createMessage = post("/describe/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("moduleId", "ghostgame-module-0.0.1-SNAPSHOT.jar");
        mockMvc.perform(createMessage)
        .andDo(print())
        .andExpect(jsonPath("$.iadescription").value("Minimax IA."))
        .andExpect(jsonPath("$.rulesDescription").value("A two players ghost words game. No bluffing plays and challenges."));
//        .andExpect()
    }

}

