package unit;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.controllers.GhostGameController;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.GhostGameModules;


public class GhostGameControllerTest {
    MockMvc mockMvc;

    @InjectMocks
    GhostGameController controller;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(controller).build();
        GhostGameModules ghostGameModules = new GhostGameModules(GhostGameModules.class.getResource("/game-modules/").getPath());
        ghostGameModules.reload();
        controller.setGameModules(ghostGameModules);
    }

    @Test
    public final void whenUserSelectToDescribeFixtureDataCheckReturnedDataIsCorrect() throws Exception {
        MockHttpServletRequestBuilder createMessage = post("/describe/")
                .param("moduleId", "ghostgame-module-0.0.1-SNAPSHOT.jar").contentType(MediaType.APPLICATION_FORM_URLENCODED);
        mockMvc.perform(createMessage)
        .andDo(print())
        .andExpect(jsonPath("$.iadescription").value("Minimax IA."))
        .andExpect(jsonPath("$.rulesDescription").value("A two players ghost words game. No bluffing plays and challenges."));
    }

    @Test
    public final void starNewGameIncorrectModuleId() throws Exception {
        MockHttpServletRequestBuilder createMessage = post("/startNewGame/").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("moduleId", "IncorrectModuleName");
        mockMvc.perform(createMessage)
        .andExpect(jsonPath("$.reason").value("There is no module named that way!"))
        .andExpect(jsonPath("$.gameEnded").value(true));
    }

    @Test
    public final void testPlay() throws Exception {
        MockHttpSession session = (MockHttpSession) mockMvc.perform(post("/startNewGame/")
                        .param("moduleId", "ghostgame-module-0.0.1-SNAPSHOT.jar")).andReturn().getRequest().getSession();

        mockMvc.perform(post("/play/")
                        .param("move", "n").session(session))
               .andDo(print())
               .andExpect(jsonPath("$.gameEnded").value(false))         
               .andExpect(jsonPath("$.currentWord").value("ny"));

        mockMvc.perform(post("/play/")
                        .param("move", "c").session(session))
                .andExpect(jsonPath("$.gameEnded").value(false))
               .andExpect(jsonPath("$.currentWord").value("nyct"));

        mockMvc.perform(post("/play/")
                .param("move", "a").session(session))
        .andExpect(jsonPath("$.gameEnded").value(false))
       .andExpect(jsonPath("$.currentWord").value("nyctal"));

        mockMvc.perform(post("/play/")
                .param("move", "o").session(session))
        .andExpect(jsonPath("$.gameEnded").value(false))
       .andExpect(jsonPath("$.currentWord").value("nyctalop"));

        
        mockMvc.perform(post("/play/")
                        .param("move", "i").session(session))
               .andDo(print())
               .andExpect(jsonPath("$.reason").value("Congratulations, you Win!"))
               .andExpect(jsonPath("$.currentWord").value("nyctalopia"))
               .andExpect(jsonPath("$.gameEnded").value(true));
    }

}
