package unit;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.GameModuleDescription;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.exceptions.NoSuchModuleException;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.web.modules.GhostGameModules;

public class GhostGameModulesTest {

    GhostGameModules gameModules;
    
    @Before
    public void setUp() {
        gameModules = new GhostGameModules();
    }
    
    @Test
    public void whenLoadOnPathCheckModulesAreLoaded() throws NoSuchModuleException {
        gameModules.load(GhostGameModules.class.getResource("/game-modules/").getPath());
        Map<String, String> factoriesIds = gameModules.getModulesIds();
        assertFalse(factoriesIds.isEmpty());
        String factoryId = factoriesIds.keySet().iterator().next(); 
        assertEquals("Minimax IA.", gameModules.getInstance(factoryId, GameModuleDescription.class).getIADescription());
    }
}
