package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

/**
 * A tree made up of ICharacterNode and built with the IDictionaryReader returned words.
 * That could be employed by several modules like IGhostPlayer and IGhostGameRulesModule. 
 */
public interface ICharacterWordTreeMap {

    /**
     * 
     * @param word the word to look up in the tree.
     * @return the node which represents that word.
     */
    <T>ICharacterNode<T> getNode(String word);

}
