package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

public class GameModuleDescription {
    private String iADescription;
    private String rulesDescription;

    public GameModuleDescription(String iaDescription, String rulesDescription) {
        this.iADescription = iaDescription;
        this.rulesDescription = rulesDescription;
    }

    public String getIADescription() {
        return iADescription;
    }
    public String getRulesDescription() {
        return rulesDescription;
    }
}
