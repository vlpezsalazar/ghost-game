package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.exceptions;

public class NoSuchWordException extends Exception {

    private static final long serialVersionUID = 1L;

    public NoSuchWordException(String word) {
        super("The dictionary doesn't contain a word like " + word);
    }

}
