package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

import java.io.IOException;
import java.util.Set;

import javax.activation.DataSource;

/**
 * This interface abstracts the data access objects. 
 * @author victor
 *
 */
public interface IDictionaryReader {
	/**
	 * Reads a dictionary from the configured data source.
	 * @return the set of words that the source contains.  
	 */
	Set<String> read() throws Throwable;
	
	/**
	 * Sets the data source where the dictionary data should be retrieved.
	 * @param dataSource the data source where the dictionary data should be retrieved.
	 * @throws IOException 
	 */
    void setDataSource(DataSource dataSource) throws IOException;
}
