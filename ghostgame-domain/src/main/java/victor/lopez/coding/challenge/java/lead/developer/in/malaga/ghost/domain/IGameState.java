package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

/**
 * This models the possible game states.
 * @author victor
 *
 */
public interface IGameState {
    /**
     * 
     * @return true if the game is in a state which could not be continued:
     */
    boolean isGameEnded();
    /**
     * @return a string in natural language explaining why the game
     * has entered in this state. 
     */
    String getReason();
    
    /**
     * 
     * @return the word built up to the moment.
     */
    String getCurrentWord();
}
