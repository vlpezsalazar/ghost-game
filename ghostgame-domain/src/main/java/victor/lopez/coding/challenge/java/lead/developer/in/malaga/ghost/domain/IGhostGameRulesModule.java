package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

/**
 * Module that takes care of game rules and ending condition of a ghost game.
 * 
 * @author Víctor Lopez Salazar
 *
 */
public interface IGhostGameRulesModule {

    /**
     * Starts a new game.
     * @return the game status after starting the game  
     */
	IGameState startNewGame();

	/**
	 * @param c the next character of the current word.
	 * @return the GameStatus after the last player movement. 
	 */
    IGameState nextTurn(String c);

    /**
     * 
     * @return the minimum word length needed to meet the endingCondition.
     */
    int getMinimumAcceptedWordLength();

    /**
     * Check if a word meets an ending condition for the current ghost game.
     * @return true if the word meets the condition and the game must end, false in any other case.
     */
    boolean checkEndingCondition(String word);
    
    /**
     * @return the word built up to the moment.
     */
    String getCurrentWord();

    /**
     * 
     * @return the current character tree for the dictionary that this game is using.
     */
    ICharacterWordTreeMap getCharacterWordTree();

    /**
     * 
     * @return a descriptive text in natural language informing about the rules
     * that this module implements.
     */
    String getDescription();

    /**
     * This method would always be called after checkEndingCondition was called.
     *   
     * @return a descriptive text with the reason why the ending condition was
     *           true.
     */
    String getReason();

}
