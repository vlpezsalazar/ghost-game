package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

import java.util.List;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.exceptions.NoSuchWordException;

/**
 * An interface for a ghost player. It is used to abstract the
 * different players implementations. 
 *
 */
public interface IGhostPlayer {

    /**
     * Play a game starting from a word and returning the character played.
     * @param currentWord the word built up to the moment which the user has
     * to play from.
     * @return a String with the next letter to play.
     * @throws NoSuchWordException  if the word couldn't be extended, i.e. the word
     * is not in the dictionary. 
     */
	String play(String currentWord) throws NoSuchWordException;

	/**
	 * 
	 * @param word the word to play from.
	 * @return the next possible words according to the implemented player strategy. 
	 * @throws NoSuchWordException if the word couldn't be extended, i.e. the word
	 * is not in the dictionary. 
	 * 
	 */
	List<String> getNextPlayingSet(String word) throws NoSuchWordException;

	/**
	 * 
	 * @return
	 */
	String getDescription();
	/**
	 * 
	 * @return the rules that this ghost player is using.
	 */
    IGhostGameRulesModule getRules();

}
