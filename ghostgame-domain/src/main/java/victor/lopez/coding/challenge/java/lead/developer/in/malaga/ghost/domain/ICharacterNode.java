package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain;

import java.util.Collection;

/**
 * A word - character node. This interface could be employed to abstract
 * the representation of a node in ICharacterWordTreeMap. 
 *
 */
public interface ICharacterNode<T> extends Comparable<ICharacterNode<T>>{

    /**
     * 
     * @return the childs of this node, i.e. the nodes with the same root word 
     *         and a length == this.getWordLength()+1, or null if this node doesn't
     *         have any child.
     */
    Collection<ICharacterNode<T>> getChilds();

    /**
     * 
     * @return the word this node represents 
     */
    String getUpToWord();

    /**
     * 
     * @return the length of word that this node represents 
     */
    int getWordLength();
    
    /**
     * 
     * @return true if this words is a meaningful word (one of the words returned by IDictionaryReader) 
     */
    boolean isWord();

    /**
     * 
     * @return the current character of the node.
     */
    T getCharacter();

    /**
     * 
     * @return the node's parent in the tree, i.e. the node with length == this.getWordLength()-1 and
     * this.getUpToWord().startsWith(this.getParent().getUpToWord())  
     */
    ICharacterNode<T> getParent();

    /**
     * 
     * @param currentCharacter an alphabet letter. 
     * @return the child with the word getUpToWord() + currentCharacter
     *         and length == this.getWordLength()+1 if that child exists
     *         or null in any other case.
     */
    ICharacterNode<T> getChild(T currentCharacter);

    /**
     * Adds a child of this node with the word getUpToWord() + currentCharacter
     * and length == this.getWordLength()+1 and returns the child created.
     * 
     * @param currentCharacter the character to add as a child of this node.
     * @param isWord true if the child is a complete word, present in the words
     *        returned by IDictionaryReader
     * @return the child created;
     */
    ICharacterNode<T> addChild(T currentCharacter, boolean isWord);

    /**
     * 
     * @return true if this
     */
    boolean hasChilds();
}
