# Ghost Word Game #

This project is composed by three subprojects:

* game-module: several interfaces to model an abstract word game.
* game-domain: classes implementing the game domain.
* game-web: spring web application with a simple UI.

To create a deployable war run _mvn package install_ and move the generated war to the webapp folder of your servlet container.