**Ghost Game Module implementation**

This module implements a simple two players  ghost game without challenges and bluffing plays and provides a Minimax player. It is intended for use with the ghostgame-web module.
