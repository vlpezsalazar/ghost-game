package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import com.google.inject.Inject;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGameState;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterNode;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;

public class GhostGameRulesModule implements IGhostGameRulesModule{

    private ICharacterWordTreeMap map;
    private StringBuffer currentWord;
    private boolean ended;
    private String reason;
    
    @Inject
	public GhostGameRulesModule(ICharacterWordTreeMap map){
        this.map = map;
        currentWord = new StringBuffer();
        ended = false;
	}

    public IGameState nextTurn(String c) {
        IGameState status; 
        if (!ended) {
            ended = checkEndingCondition(currentWord.append(c).toString());
            if (ended) {
                if (currentWord.length() % 2 == 1) {
                    status = new TwoPlayersGameStatus(currentWord.toString(), TwoPlayersGameStatus.YOU_LOSE + reason, true);
                } else status = new TwoPlayersGameStatus(currentWord.toString(), TwoPlayersGameStatus.YOU_WIN, true);
            } else {
                status = new TwoPlayersGameStatus(currentWord.toString(), TwoPlayersGameStatus.OK, false);
            }
        } else {
            status = new TwoPlayersGameStatus(currentWord.toString(), TwoPlayersGameStatus.GAME_FINISHED, true);
        }
        return status;
    }

    public String getCurrentWord() {
        return currentWord.toString();
    }

	@Override
	public IGameState startNewGame() {
	    reason = "";
	    ended = false;
		currentWord.delete(0, currentWord.length());
		return new TwoPlayersGameStatus(currentWord.toString(), TwoPlayersGameStatus.OK, false);
	}

    @Override
    public int getMinimumAcceptedWordLength() {
        return 4;
    }

    @Override
    public boolean checkEndingCondition(String word) {
        ICharacterNode<Alphabet> node = map.getNode(word);

        if (node == null) {
            reason = " That letter couldn'complete any correct word.";
        } else if (node.isWord() && word.length() >= getMinimumAcceptedWordLength()){
            reason = " You have completed a word!";
        }
        return node == null || (node.isWord() && word.length() >= getMinimumAcceptedWordLength());
    }

    @Override
    public ICharacterWordTreeMap getCharacterWordTree() {
        return map;
    }

    @Override
    public String getDescription() {
        return "In this game, two players take turns building up an English word from left to right. Each player adds one letter per turn. The goal is to not complete the spelling of a word: if you add a letter that completes a word (of 4+ letters), or if you add a letter that produces a string that cannot be extended into a word, you lose.";
    }

    @Override
    public String getReason() {
        return reason;
    }

}
