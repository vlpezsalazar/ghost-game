/**
 * Ghost Game Module implementation. This module implements a simple two players
 * ghost game without challenges and bluffing plays.
 * 
 * The package contains the game rules and data structures of the module.
 * 
 */
package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;