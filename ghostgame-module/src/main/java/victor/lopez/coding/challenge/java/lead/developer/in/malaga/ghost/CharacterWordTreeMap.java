package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import java.util.EnumMap;
import java.util.Set;

import com.google.inject.Inject;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterNode;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;
/**
 * This class creates a tree composed by characters. Each branch from the root to a leaf made a dictionary word.
 * That is useful for automated players and ghosts modules rules holders.
 *   
 */
public class CharacterWordTreeMap implements ICharacterWordTreeMap {

    private EnumMap<Alphabet, ICharacterNode<Alphabet>> characterTreeRoots;
    private Alphabet[] alphabet;

    @Inject
    public CharacterWordTreeMap(IDictionaryReader dictionaryReader) throws Throwable {
        Set<String> words = dictionaryReader.read();
        alphabet = Alphabet.values();
        characterTreeRoots = new EnumMap<Alphabet, ICharacterNode<Alphabet>>(Alphabet.class);
        for (String word : words) {
            createBranch(word);
        }
        
    }

    private void createBranch(String word) {

        Alphabet currentCharacter = alphabet[word.charAt(0) - 'a'];
        ICharacterNode<Alphabet> characterNode = characterTreeRoots.get(currentCharacter);
        if (characterNode == null) {
            characterNode = new CharacterNode(currentCharacter, null, 1, word.length() == 1);
            characterTreeRoots.put(currentCharacter, characterNode);
        }

        for (int i = 1, wordLength = word.length(); i < wordLength; i++) {
            //find the current node
            currentCharacter = alphabet[word.charAt(i) - 'a'];
            ICharacterNode<Alphabet> nextCharacterNode = characterNode.getChild(currentCharacter);
            //check if the node has the corresponding child
            if (nextCharacterNode == null) {
                nextCharacterNode = characterNode.addChild(currentCharacter, i == wordLength-1);
            }
            characterNode = nextCharacterNode;
        }
        
    }

    @SuppressWarnings("unchecked")
    @Override
    public ICharacterNode<Alphabet> getNode(String word) {
        ICharacterNode<Alphabet> characterNode = null;
        try {
            Alphabet currentCharacter = alphabet[word.charAt(0) - 'a'];
            characterNode = characterTreeRoots.get(currentCharacter);
    
            for (int i = 1, wordLength = word.length(); i < wordLength && characterNode != null; i++) {
                //find the current character of the word
                currentCharacter = alphabet[word.charAt(i) - 'a'];
                
                //get the node corresponding to that character
                characterNode = characterNode.getChild(currentCharacter);
            }
        } catch (IndexOutOfBoundsException e) {}
        
        return characterNode;
    }
}
