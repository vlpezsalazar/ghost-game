package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.inject.Inject;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.Alphabet;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterNode;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostPlayer;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.exceptions.NoSuchWordException;
/**
 * A stateless agent for playing the ghost game by means of min max algorithm. 
 * 
 * @author Víctor López Salazar
 *
 */
public class StatelessRecursiveMinMaxRandomGhostPlayer implements IGhostPlayer {

    private ICharacterWordTreeMap map;
    private Random randomGenerator;
    private IGhostGameRulesModule rules;

    @Inject
    public StatelessRecursiveMinMaxRandomGhostPlayer(IGhostGameRulesModule rules) {
        this.rules = rules;
        this.map = rules.getCharacterWordTree();
        randomGenerator = new Random(System.currentTimeMillis());
    }

    @Override
    public String play(String currentWord) throws NoSuchWordException {
        if (currentWord == null || currentWord.isEmpty()) currentWord = "a"; //select a default starting character.
        int wordLength = currentWord.length();
        List<String> bestPossibleMoves = getNextPlayingSet(currentWord);
        return bestPossibleMoves.get(randomGenerator.nextInt(bestPossibleMoves.size())).substring(wordLength, wordLength+1);
    }

    @Override
    public List<String> getNextPlayingSet(String word) throws NoSuchWordException {
        ICharacterNode<Alphabet> node = map.getNode(word);
        List<String> nextPlayingSet = new ArrayList<String>();
        if (node != null) {
            int playerTurn = (word.length() + 1) % 2;
            TreeSet<ICharacterNode<Alphabet>> bestMoves = getNextPlayingSet(new TreeSet<ICharacterNode<Alphabet>>(), node, playerTurn, playerTurn);
            if (bestMoves.isEmpty()) throw new NoSuchWordException(word);
            NavigableSet<ICharacterNode<Alphabet>> winningOrLossing = bestMoves;
            int wordLength = bestMoves.first().getWordLength();

            // if computer loose, select the longest moves
            if (!playerWins(playerTurn, wordLength)){
                winningOrLossing = bestMoves.descendingSet();
                wordLength = winningOrLossing.first().getWordLength();
                        
            }
            for (ICharacterNode<Alphabet> possibleMove : winningOrLossing) {
                if (wordLength == possibleMove.getWordLength()) {
                    nextPlayingSet.add(possibleMove.getUpToWord());
                } else break;
            }
        }

        return nextPlayingSet;
    }
    
    private TreeSet<ICharacterNode<Alphabet>> getNextPlayingSet(TreeSet<ICharacterNode<Alphabet>> playerPlayingSet, ICharacterNode<Alphabet> currentNode, int whoami, int playerTurn){
        if (currentNode != null) {
            int currentMinimumSolutionLength = getMinimumWinningMovesLength(playerPlayingSet, whoami);
          //if the computer player already has a better solution than the current node, prune it and forget about player turn
            if (currentMinimumSolutionLength == -1 || currentNode.getWordLength() < currentMinimumSolutionLength) {
                for (ICharacterNode<Alphabet> child : currentNode.getChilds()) {
                    if (rules.checkEndingCondition(child.getUpToWord())) {
                        playerPlayingSet.add(child);
                    } else if (child.hasChilds()){
                        //recursive descent is enough and easier: this game is not prone to make any stack overflow 
                        //errors due to the maximum word length (it is about 30), so I've chosen this approach 
                        //instead of the iterative one that uses a Stack and a while(!stack.isEmpty()) to maintain the
                        //state in each cycle.
                        TreeSet<ICharacterNode<Alphabet>> branchPlayingSet = getNextPlayingSet(new TreeSet<ICharacterNode<Alphabet>>(), child, whoami, (playerTurn + 1) % 2);
                        playerPlayingSet.addAll(branchPlayingSet);
                    }
                }
                if (!playerPlayingSet.isEmpty()) {
                    if (hasWinningMoves(playerPlayingSet, playerTurn)) {
                        //select those nodes that lead the current player to win with the minimum length, if there is any.
                        removeLosserAndMaxLengthMoves(playerPlayingSet, playerTurn);
                    } else {
                        //in case there are no winning moves only save those with the max length
                        removeMinimumLengthMoves(playerPlayingSet);
                    }
                }
            }

        }
        return playerPlayingSet;
    }

    private void removeMinimumLengthMoves(TreeSet<ICharacterNode<Alphabet>> playerPlayingSet) {
        int wordLength = playerPlayingSet.last().getWordLength();
        Iterator<ICharacterNode<Alphabet>> iterator = playerPlayingSet.iterator();
        for (ICharacterNode<Alphabet> node = iterator.next(); wordLength > node.getWordLength(); node = iterator.next()) {
            iterator.remove();
        }
    }

    private TreeSet<ICharacterNode<Alphabet>> removeLosserAndMaxLengthMoves(TreeSet<ICharacterNode<Alphabet>> playerPlayingSet, int playerTurn) {
        int minWordLength = Integer.MAX_VALUE;
        Iterator<ICharacterNode<Alphabet>> playerMovesSetIterator = playerPlayingSet.iterator();
        while (playerMovesSetIterator.hasNext()) {
            int wordLength = playerMovesSetIterator.next().getWordLength();
            if (!playerWins(playerTurn, wordLength) || wordLength > minWordLength){
                playerMovesSetIterator.remove();
            } else if (wordLength < minWordLength) {
                minWordLength = wordLength;
            }
        }
        return playerPlayingSet;
    }

    private boolean hasWinningMoves(TreeSet<ICharacterNode<Alphabet>> nextPlayingSet, int playerTurn) {
        for (ICharacterNode<Alphabet> node : nextPlayingSet) {
            int wordLength = node.getWordLength();
            if (playerWins(playerTurn, wordLength)){
                return true;
            }
        }
        return false;
    }

    private int getMinimumWinningMovesLength(SortedSet<ICharacterNode<Alphabet>> nextPlayingSet, int playerTurn) {
        for (ICharacterNode<Alphabet> node : nextPlayingSet) {
            int wordLength = node.getWordLength();
            if (playerWins(playerTurn, wordLength)){
                    return wordLength;
            }
        }
        return -1;
    }
    private boolean playerWins(int playerTurn, int wordLength) {
        return ((playerTurn == 0 && wordLength % 2 == 1) ||
                (playerTurn == 1 && wordLength % 2 == 0));
    }

    @Override
    public IGhostGameRulesModule getRules() {
        return rules;
    }

    @Override
    public String getDescription() {
        return "A min max stateless recursive player. It should play optimally given the attached dictionary.";
    }
}
