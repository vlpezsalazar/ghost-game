package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;


public class DefaultWordListDataSource implements DataSource {
    @Override
    public String getContentType() {
        return "text/plain";
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return DefaultWordListDataSource.class.getResourceAsStream(getName());
    }

    @Override
    public String getName() {
        return "/word.lst";
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return null;
    }

}
