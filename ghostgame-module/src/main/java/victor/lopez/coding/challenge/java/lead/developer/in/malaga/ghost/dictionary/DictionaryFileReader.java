package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.TreeSet;

import javax.activation.DataSource;

import com.google.inject.Inject;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;

public class DictionaryFileReader implements IDictionaryReader {

    private static final int BUFFERLENGTH = 4096;
    
    private InputStream fis;
    @Inject
    public DictionaryFileReader(DataSource dataSource) throws IOException{
    	fis = dataSource.getInputStream();
    }
	public Set<String> read() throws IOException {
    	Set<String> words;
    	try {
			byte []buffer = new byte[BUFFERLENGTH];
			String lastWord = null;
		    words = new TreeSet<String>();
		    StringBuffer textChunk = new StringBuffer();
		    for (int readed = 0; (readed = fis.read(buffer, 0, BUFFERLENGTH)) != -1; textChunk.delete(0, textChunk.length())){
		    	if (lastWord != null){
		    		textChunk.append(lastWord.toLowerCase());
		    	}
		    	textChunk.append(new String(buffer, 0, readed));
		    	lastWord = fectchAndAddWordsToSet(textChunk.toString(), words);
		    }
    	}finally {
    		if (fis != null){
    			fis.close();
    		}
    	}
    	return words;
    }

	private String fectchAndAddWordsToSet(String  text, Set<String> words) {
		String[] wordsArray = text.split("\n|\r\n");
		int n = text.endsWith("\n") ? wordsArray.length : wordsArray.length -1;
		for (int i = 0; i < n; i++){
			if (!wordsArray[i].isEmpty()){
				words.add(wordsArray[i]);
			}
		}
		return n < wordsArray.length ? wordsArray[n] : null;
	}

	@Override
	public void setDataSource(DataSource dataSource) throws IOException {
		fis = dataSource.getInputStream();
		
	}

}
