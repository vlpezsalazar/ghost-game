package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.GameModuleDescription;

public class TwoPlayersModuleDescription extends GameModuleDescription {

    public TwoPlayersModuleDescription() {
        super("Minimax IA.", "A two players ghost words game. No bluffing plays and challenges.");
    }

}
