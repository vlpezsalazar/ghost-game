package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGameState;

public class TwoPlayersGameStatus implements IGameState {

    public static final String OK = "Ok, you can go on...";
    public static final String GAME_FINISHED = "The game has already finished.";
    public static final String YOU_WIN = "Congratulations, you Win!";
    public static final String YOU_LOSE = "Sorry... but you lose this time.";

    private String reason;
    private boolean ended;
    private String currentWord;
    
    TwoPlayersGameStatus(){}
    TwoPlayersGameStatus(String currentWord, String reason, boolean ended) {
        this.currentWord = currentWord;
        this.reason = reason;
        this.ended = ended;
    }
    @Override
    public String getReason() {
        return reason;
    }
    @Override
    public String getCurrentWord() {
        return currentWord;
    }
    @Override
    public boolean isGameEnded() {
        return ended;
    }
}
