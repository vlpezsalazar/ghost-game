package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

public enum Alphabet {
    a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
}
