package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import java.util.Collection;
import java.util.EnumMap;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterNode;

public class CharacterNode implements ICharacterNode<Alphabet> {
    private static StringBuffer buf = new StringBuffer();
    private Alphabet character;
    private EnumMap<Alphabet, ICharacterNode<Alphabet>> childs;
    private ICharacterNode<Alphabet> parent;
    private int wordLength;
    private boolean isWord;
    private String upToWord;
    
    public CharacterNode(Alphabet character, CharacterNode parent, int length, boolean isWord){
        this.character = character;
        this.parent = parent;
        this.wordLength = length;
        buf.delete(0, buf.length());
        buf.append(character);
        ICharacterNode<Alphabet> localParent = parent;
        while (localParent !=null) {
            buf.insert(0, localParent.getCharacter());
            localParent = localParent.getParent();
        }
        this.upToWord = buf.toString();   
        this.isWord = isWord;
        childs = null;
    }
    public ICharacterNode<Alphabet> addChild(Alphabet character, boolean isWord) {
        if (childs == null) {
            childs = new EnumMap<Alphabet, ICharacterNode<Alphabet>>(Alphabet.class);
        }
        CharacterNode characterNode = new CharacterNode(character, this, wordLength+1, isWord);
        childs.put(character, characterNode);
        return characterNode;
    }
    public Alphabet getCharacter() {
        return character;
    }
    public String getUpToWord() {
        return upToWord;
    }
    @Override
    public int getWordLength() {
        return wordLength;
    }
    public ICharacterNode<Alphabet> getParent() {
        return parent;
    }
    public ICharacterNode<Alphabet> getChild(Alphabet currentCharacter) {
        return childs != null ? childs.get(currentCharacter) : null;
    }
    @Override
    public boolean isWord() {
        return isWord;
    }
    @Override
    public int compareTo(ICharacterNode<Alphabet> o) {
        int cmp = upToWord.length() - o.getUpToWord().length(); 
        if (cmp == 0) cmp = upToWord.compareTo(o.getUpToWord());
        return cmp;
    }
    @Override
    public Collection<ICharacterNode<Alphabet>> getChilds() {
        return childs != null ? childs.values() : null;
    }
    @Override
    public String toString() {
        return upToWord;
    }
    @Override
    public boolean hasChilds() {
        return childs != null;
    }
}
