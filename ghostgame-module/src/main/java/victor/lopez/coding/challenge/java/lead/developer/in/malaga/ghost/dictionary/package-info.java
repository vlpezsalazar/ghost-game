/**
 * Dictionary reader implementation. This package contains the dictionary reader
 * implementation for IDictionaryReader.
 * 
 */
package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary;