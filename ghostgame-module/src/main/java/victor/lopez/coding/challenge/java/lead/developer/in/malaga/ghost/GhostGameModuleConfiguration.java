package victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost;

import javax.activation.DataSource;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary.DictionaryFileReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.GameModuleDescription;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGameState;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostPlayer;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.player.StatelessRecursiveMinMaxRandomGhostPlayer;

import com.google.inject.AbstractModule;

public class GhostGameModuleConfiguration extends AbstractModule {

    @Override
    protected void configure() {
        bind(GameModuleDescription.class).to(TwoPlayersModuleDescription.class);
        bind(DataSource.class).to(DefaultWordListDataSource.class);
        bind(IDictionaryReader.class).to(DictionaryFileReader.class);
        bind(IGameState.class).to(TwoPlayersGameStatus.class);
        bind(IGhostGameRulesModule.class).to(GhostGameRulesModule.class);
        bind(IGhostPlayer.class).to(StatelessRecursiveMinMaxRandomGhostPlayer.class).asEagerSingleton();
        bind(ICharacterWordTreeMap.class).to(CharacterWordTreeMap.class).asEagerSingleton();
    }

}
