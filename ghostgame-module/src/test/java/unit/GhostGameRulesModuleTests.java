package unit;
import static org.junit.Assert.*;

import javax.activation.DataSource;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.CharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.GhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.TwoPlayersGameStatus;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary.DictionaryFileReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;

 /**
  * Tests have been coded first. The name of each test is related with the requirement it 
  * fulfills (the requirement is left as a comment above the test, but it 
  * shouldn"t be necessary).
  * 
  */
public class GhostGameRulesModuleTests {

    private static Injector injector;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DataSource.class).to(DefaultWordListDataSourceTest.class);
                bind(IDictionaryReader.class).to(DictionaryFileReader.class);
                bind(IGhostGameRulesModule.class).to(GhostGameRulesModule.class);
                bind(ICharacterWordTreeMap.class).to(CharacterWordTreeMap.class).asEagerSingleton();
            }
        });
        
    }
    // In the game of Ghost, two players take turns building up an English word from left to right.
    @Test
    public final void whenUsersAddsOneLetterInTurnCheckCurrentWordIsBuiltLeftToRightUntilGameFinishes(){
        IGhostGameRulesModule ghostGame = injector.getInstance(IGhostGameRulesModule.class);

        //ligament
        ghostGame.nextTurn("l");
        assertEquals("l", ghostGame.getCurrentWord());
        ghostGame.nextTurn("i");
        assertEquals("li", ghostGame.getCurrentWord());
        ghostGame.nextTurn("g");
        assertEquals("lig", ghostGame.getCurrentWord());
        ghostGame.nextTurn("a");
        assertEquals("liga", ghostGame.getCurrentWord());
        ghostGame.nextTurn("m");
        assertEquals("ligam", ghostGame.getCurrentWord());
        ghostGame.nextTurn("e");
        assertEquals("ligame", ghostGame.getCurrentWord());
        ghostGame.nextTurn("n");
        assertEquals("ligamen", ghostGame.getCurrentWord());
        assertEquals(TwoPlayersGameStatus.YOU_WIN, ghostGame.nextTurn("t").getReason());
        assertEquals("ligament", ghostGame.getCurrentWord());
    }

    // The goal is to not complete the spelling of a word.
    //if you add a letter that completes a word of 4+ letters, you lose.
    @Test
    public final void whenFourWordLengthIsReachedCheckTheGameCanFinish(){
        IGhostGameRulesModule ghostGame = injector.getInstance(IGhostGameRulesModule.class);

        //aalii
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("a").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("a").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("l").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("i").getReason());
        assertTrue(ghostGame.nextTurn("i").getReason().startsWith(TwoPlayersGameStatus.YOU_LOSE));

    }
    //if you add a letter that produces a string that cannot be extended into a word, you lose.
    @Test
    public final void whenUserAddsALetterThatProducesAStringThatCannotBeExtendedIntoAWordCheckGameEnds(){
        IGhostGameRulesModule ghostGame = injector.getInstance(IGhostGameRulesModule.class);
        //aalii
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("a").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("a").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("l").getReason());
        assertEquals(TwoPlayersGameStatus.OK, ghostGame.nextTurn("i").getReason());
        assertTrue(ghostGame.nextTurn("a").getReason().startsWith(TwoPlayersGameStatus.YOU_LOSE));
    }
}
