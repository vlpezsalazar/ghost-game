package unit;

import static org.junit.Assert.*;

import javax.activation.DataSource;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.Alphabet;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.CharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary.DictionaryFileReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterNode;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;

public class CharacterWordTreeMapTest {
    private static Injector injector;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DataSource.class).to(DefaultWordListDataSourceTest.class);
                bind(IDictionaryReader.class).to(DictionaryFileReader.class);
                bind(ICharacterWordTreeMap.class).to(CharacterWordTreeMap.class);
            }
        });
        
    }
    @Test
    public final void whenCharacterWordTreeMapIsBuiltCheckNodesHasTheCorrectLengthWordAndStructure(){
        ICharacterWordTreeMap characterWordTreeMap = injector.getInstance(ICharacterWordTreeMap.class);

        ICharacterNode<Alphabet> characterNode = characterWordTreeMap.getNode("aah");
        assertEquals("aah", characterNode.getUpToWord());
        assertEquals(3, characterNode.getWordLength());
        assertTrue(characterNode.isWord());

        characterNode = characterWordTreeMap.getNode("libertarian");
        assertEquals("libertarian", characterNode.getUpToWord());
        assertTrue(characterNode.isWord());
        assertEquals(11, characterNode.getWordLength());

        characterNode = characterNode.getParent();
        assertEquals("libertaria", characterNode.getUpToWord());
        assertFalse(characterNode.isWord());
        assertEquals(10, characterNode.getWordLength());

    }
}
