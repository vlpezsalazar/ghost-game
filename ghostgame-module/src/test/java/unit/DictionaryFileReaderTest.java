package unit;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import javax.activation.DataSource;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary.DictionaryFileReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;

public class DictionaryFileReaderTest {

    private IDictionaryReader dictionaryFileReader;
    private static Injector injector;

	@BeforeClass
    public static void setUpBeforeClass() throws Exception {
	    injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DataSource.class).to(DefaultWordListDataSourceTest.class);
                bind(IDictionaryReader.class).to(DictionaryFileReader.class);

            }
        });
	}
    @Test
    public final void whenDictionaryIsLoadedCheckItActuallyContainsAnOrderedSetAndTheCorrectNumberOfWords() throws Throwable{
        dictionaryFileReader = injector.getInstance(IDictionaryReader.class);
                
        Set<String> wordset = dictionaryFileReader.read();
        Iterator<String> iterator = wordset.iterator();
        assertEquals("aa", iterator.next());
        assertEquals("aah", iterator.next());
        assertEquals("aahed", iterator.next());
        assertEquals("aahing", iterator.next());
        assertEquals("aahs", iterator.next());
        assertEquals("aal", iterator.next());
        assertEquals("aalii", iterator.next());
        assertEquals("aaliis", iterator.next());
        assertEquals("aals", iterator.next());
        assertEquals(173528, wordset.size());
    }
}
