package unit;

import static org.junit.Assert.*;

import java.util.List;

import javax.activation.DataSource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.Alphabet;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.CharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.GhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.dictionary.DictionaryFileReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.ICharacterWordTreeMap;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IDictionaryReader;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostGameRulesModule;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.IGhostPlayer;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.domain.exceptions.NoSuchWordException;
import victor.lopez.coding.challenge.java.lead.developer.in.malaga.ghost.player.StatelessRecursiveMinMaxRandomGhostPlayer;

public class StatelessRecursiveMinMaxRandomGhostPlayerTest {
    private static Injector injector;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DataSource.class).to(DefaultWordListDataSourceTest.class);
                bind(IDictionaryReader.class).to(DictionaryFileReader.class);
                bind(IGhostGameRulesModule.class).to(GhostGameRulesModule.class);
                bind(IGhostPlayer.class).to(StatelessRecursiveMinMaxRandomGhostPlayer.class).asEagerSingleton();
                bind(ICharacterWordTreeMap.class).to(CharacterWordTreeMap.class);
            }
        });
        
    }
    private IGhostPlayer computerPlayer;
    
    @Before
    public void setUp() {
        computerPlayer = injector.getInstance(IGhostPlayer.class);
    }
    
//  * The computer should play optimally given the attached dictionary.
    @Test
    public final void whenUserSelectsOneLossingMoveCheckTheWinningMinimumLengthPathIsSelected() throws NoSuchWordException{
        assertEquals("l", computerPlayer.play("l"));
        assertEquals("a", computerPlayer.play("ll"));
        String move = computerPlayer.play("lla");
        assertTrue("m".equals(move) || "n".equals(move));
    }
    
//  * If the computer thinks it will win, it should play randomly among all its winning moves; 
    @Test
    public final void whenComputerHasWinningMovesCheckItsPlayingSetContainsSeveralMinimumPathsWords() throws NoSuchWordException{
        List<String> nextPlayingSet = computerPlayer.getNextPlayingSet("oak");
        assertTrue(nextPlayingSet.contains("oaken") && nextPlayingSet.contains("oakum"));        
    }

//  * if the computer thinks it will lose, it should play so as to extend the game as long as possible
//  * choosing randomly among choices that force the maximal game length.
    @Test
    public final void whenComputerCannotWinCheckItSelectsTheLongestPath() throws NoSuchWordException{
        int nyctalopia = 10;

        assertArrayEquals(new String[] {"nyctalopia"}, computerPlayer.getNextPlayingSet("n").toArray());
        
        for (Alphabet letter : Alphabet.values()) {
            if (letter != Alphabet.y) {
                try {
                    List<String> nset = computerPlayer.getNextPlayingSet("n" + letter);
                    for (String word : nset) {
                        assertTrue("word:" + word + " is longer than nyctalopia.", nyctalopia > word.length());
                    }
                } catch (NoSuchWordException e) {
                    //ignore the exception
                }                
            }
        }
    }
}
